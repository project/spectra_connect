<?php

namespace Drupal\spectra_connect;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a SpectraConnect entity.
 *
 * @ingroup spectra_connect
 */
interface SpectraConnectInterface extends ConfigEntityInterface {

}
